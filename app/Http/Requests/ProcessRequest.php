<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|digits_between:1,10|numeric|unique:users',
            'headquarters' => 'required|string',
            'description' => 'required|string|max:90',
            'date' => 'required|date',
            'budget' => 'required|digits_between:5,10|numeric',
            
        ];
    }
}
