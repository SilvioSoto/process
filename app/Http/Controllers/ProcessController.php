<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Process;
use App\Http\Requests;
use App\Http\Requests\ProcessRequest;
use Illuminate\Support\Facades\Auth;
use \App\User;
use App\Http\Requests\update;
class ProcessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        $Process = Process::where('state', "<>", "Eliminado")->paginate(2);
        if($request->ajax()){
            return response()->json(view('Process.ListProcess', compact('Process'))->render());
        }
       
        return view('home', compact('Process'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProcessRequest $request)
    {
        $process = new Process();
        $process->id = $request->id;
        $process->description = $request->description;
        $process->date = $request->date;
        $process->headquarters = $request->headquarters;
        $process->budget = $request->budget;
        $process->user_id = Auth::user()->id;
        $process->state = "Activo";
        $process->save();
        
        return response()->json([
            "process" => $process
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Process = Process::find($id);
       
        return response()->json([
             "process" => $Process
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Process = Process::find($id);
       
        return response()->json([
             "process" => $Process
        ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(update $request, $id)
    {
        $Process = Process::find($id);
        $Process->description = $request->description;
        $Process->date = $request->date;
        $Process->headquarters = $request->headquarters;
        $Process->budget = $request->budget;
        $Process->user_id = Auth::user()->id;
        $Process->state = "Activo";
        $Process->save();
        return response()->json([
             "process" => $Process
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
   {    
       $Process = Process::find($id);
       $Process->state="Eliminado";
       $Process->save();
        return response()->json([
            "process" => $Process
        ]);
    }

  
    public function see($id){

        $Process = Process::find($id);
        return view('Process',compact('Process'));
    }
}
