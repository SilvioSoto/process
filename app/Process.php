<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $table = "process";
    public $timestamps = false;
    public function user(){
        return $this->hasMany(User::class);
    }
}
