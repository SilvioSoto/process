@section('css')
    {!!Html::style('css/Process.css')!!}
@endsection

<button type="button" class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target=".bd-example-modal-lg">
    Crear Proceso
</button>
                   
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">Registrar Proceso</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <form  id="form-process">
        <input type = "hidden" name="_token" value="{{csrf_token()}}" id="tokenProcess">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="exampleFormControlInput1">Numero de Proceso</label>
                <input type="number" class="form-control"  name="id">
                <div class="id"></div>
            </div>
            <div class="form-group col-md-6">
                <label for="exampleFormControlInput1">Fecha</label>
                <input type="date" class="form-control" name="date">
                <div class="date"></div>
            </div>
        </div>
        
        <div class="form-row">
        <div class="form-group col-md-6">
                <label for="exampleFormControlSelect1">Sede</label>
                <select class="form-control"  name="headquarters">
                <option value="Bogota">Bogotá</option>
                <option value="Mexico">Mexico</option>
                <option value="Peru">Perú</option>
                </select>
                <div class="headquarters"></div>
            </div>
        
            <div class="form-group col-md-6">
                <label for="exampleFormControlInput1">Presupuesto</label>
                <input type="number" class="form-control" name="budget">
                <div class="budget"></div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                    <label for="exampleFormControlTextarea1">Descripción</label>
                    <textarea class="form-control"  name="description" rows="3"></textarea>
                <div class="description"></div>
                </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <a href="#" id ="send-process" class="btn btn-primary btn-lg active" role="button" >Enviar</a>
            </div>
        </div> 
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>
@section('scritps')
    {!!Html::script('js/Process/Process.js')!!}
@endsection
