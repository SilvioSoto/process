<form action="">
<div class="datos">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Descipcion</th>
      <th scope="col">Fecha</th>
      <th scope="col">Presupuesto</th>
      <th scope="col">Editar</th>
      <th scope="col">Eliminar</th>
      <th scope="col">Ver</th>
    </tr>
  </thead>
  <tbody>
  @foreach($Process as $process)
  <tbody>
    <tr>
      <td>{{$process->id}}</td>
      <td>{{$process->description}}</td>
      <td>{{$process->date}}</td>
      <td>{{$process->headquarters}}</td>
      <td><button type="button"  id="{{$process->id}}"  onclick="show('{{$process->id}}')" class="btn btn-success edit">Editar</button></td>
      <td><button type="button"  id="{{$process->id}}"  onclick="DeleteProcess('{{$process->id}}')" class="btn btn-danger eliminar">Eliminar</button></td>
      <td><a class="btn btn-primary" href="/ProcessId/{{$process->id}}"  role="button">Ver</a></td>
      
    </tr>
  </tbody>
    @endforeach
  </tbody>
</table>
<div class="datos">
{!!$Process->render()!!}
</form>