@extends('layouts.app')



@section('nav')
<a class="navbar-brand" href="/Process">Procesos</a>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Proceso {{$Process->id}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-borderless">
                        <thead>
                            <tr>
                            <th scope="col">id</th>
                            <th scope="col">Descipcion</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Presupuesto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>{{$Process->id}}</td>
                            <td>{{$Process->description}}</td>
                            <td>{{$Process->date}}</td>
                            <td>{{$Process->headquarters}}</td>
                            </tr>
                        </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
