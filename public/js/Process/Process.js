$(document).ready(function(){
    $('#send-process').click(function(){
    var ruta = window.location.origin +"/Process";
    var token = $("#tokenProcess").val();
    console.log("hola");
    $.ajax({
        url: ruta,
        type: 'POST',
        headers: {"X-CSRF-TOKEN":token },
        dataType: 'json',
        data:$('#form-process').serialize(),
        success : function(json) {
            alert("Registro exitoso");
        },
        error : function(ms) {
            console.log(ms.responseJSON.errors.id);
            if(ms.responseJSON.errors.id != null){
                $('.id').html('<p class ="Error-formulario">'+ms.responseJSON.errors.id+'</p>');
            }
            if(ms.responseJSON.errors.description != null){
                $('.description').html('<p class ="Error-formulario">'+ms.responseJSON.errors.description+'</p>');
            }
            if(ms.responseJSON.errors.date != null){
                $('.date').html('<p class ="Error-formulario">'+ms.responseJSON.errors.date+'</p>');
            }
            if(ms.responseJSON.errors.budget != null){
                $('.budget').html('<p class ="Error-formulario">'+ms.responseJSON.errors.budget+'</p>');
            }
          },
    });
    })

    $('#edit-process').click(function(){
        Edit();
    });

});

function Edit(){
    var id = $('#id').val();
    var ruta = window.location.origin +"/Process/"+id;
    var token = $("#tokenProcess").val();
    $.ajax({
        url: ruta,
        type: 'put',
        headers: {"X-CSRF-TOKEN":token },
        dataType: 'json',
        data:{
            id: $('#id').val(),
            description:$('#description').val(),
            budget: $('#budget').val(),
            date:$('#date').val(),
            headquarters:$('#headquarters').val(),
        },
        success : function(json) {
            console.log($('#form-process').serialize());
            alert("Registro exitoso");
        },
        error : function(ms) {
    
            alert("erorr");
            if(ms.responseJSON.errors.id != null){
                $('.id').html('<p class ="Error-formulario">'+ms.responseJSON.errors.id+'</p>');
            }
            if(ms.responseJSON.errors.description != null){
                $('.description').html('<p class ="Error-formulario">'+ms.responseJSON.errors.description+'</p>');
            }
            if(ms.responseJSON.errors.date != null){
                $('.date').html('<p class ="Error-formulario">'+ms.responseJSON.errors.date+'</p>');
            }
            if(ms.responseJSON.errors.budget != null){
                $('.budget').html('<p class ="Error-formulario">'+ms.responseJSON.errors.budget+'</p>');
            }
          },
    });
}

function DeleteProcess(id){
    var identificador = id
  
    var token = $("#tokenProcess").val();
    $.ajax({
        url: window.location.origin + "/Process/"+identificador,
        type: 'DELETE',
        headers: {"X-CSRF-TOKEN":token },
        dataType: 'json',
        data: {id:identificador},
        
        success : function(json) {
          alert("se ha eliminado Correctamente");
        },
        error : function(xhr, status) {
           
        },
    });
}

function show(id){
    var identificador = id
    var token = $("#tokenProcess").val();
    $.ajax({
        url: window.location.origin + "/Process/"+identificador,
        type: 'GET',
        headers: {"X-CSRF-TOKEN":token },
        dataType: 'json',
        data: {id:identificador},
        success : function(json) {
            $('.edit-modal').modal('show')
         
          $('#id').val(json.process.id);
          $('#description').val(json.process.description);
          $('#budget').val(json.process.budget);
          $('#date').val(json.process.date);
          $('#headquarters').val(json.process.headquarters);

        },
        error : function(ms) {
           
        },
    });
}

$( document ).on('click', '.pagination a', function(e){
    e.preventDefault();
    var page = $(this).attr('href').split('page=')[1];

    $.ajax({
        url: window.location.origin + "/Process",
        type: 'GET',
        dataType: 'json',
        data: {page: page},
        success : function(json) {
            $('.datos').html(json);
        }, 
    });
});